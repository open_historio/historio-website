<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr">
	<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Security-Policy" content="worker-src 'self' blob: default-src * 'self' 'unsafe-inline' 'unsafe-eval' data: gap: content:">
<link rel="shortcut icon" href="web/favicon.png"/>
<link rel='stylesheet' href='web/hs.css'>
	<title><?php print $skin['title'] ?></title>
<!-- scripts -->
<?php print $skin['header']['fileScripts'] ?>
<?php print $skin['fileCss'] ?>
<?php print $skin['textCss'] ?>
		</head>
		<body>
<div id='header'>
	<a href="/">Home</a>
</div>
<hr />

<div id="globalMessages">
	<?= $skin['messages']['err'] ?>
	<?= $skin['messages']['info'] ?>
	<?= $skin['messages']['warning'] ?>
</div>

