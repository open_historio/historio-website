<?php

/**
 * Boot up procedure
 */
function hs_bootMeUp()
{
    grace_debug("hs is in the house!");
}


function hs_init()
{
    $paths = array(

        # See the front page
        # @return error | frontPage
        # r=hs_front_page
        array(
            'r' => 'hs_front_page',
            'title' => 'Story of the world',
            'action' => 'hs_frontPage',
            'access' => 'users_openAccess',
            'replyType' => 'skin'
        ),

        # Load a story's main page
        # @return error | story
        # r=hs_story
        # @storyId the id of the story
        array(
            'r' => 'hs_story',
            'title' => 'View a story',
            'action' => 'hs_story',
            'access' => 'users_openAccess',
            'replyType' => 'skin',
            'params' => array(
                array('key' => 'storyId', 'req' => true),
            ),
        ),

        # Add new history
        # @return form to add a new history
        # r=hs_story_new
        array(
            'r' => 'hs_story_new',
            'title' => 'Add a new story',
            'action' => 'hs_storyNew',
            'replyType' => 'skin',
            'file' => 'storyNew.php',

        ),

        # Add new event
        # @return form to add a new event
        # r=hs_event_new
        array(
            'r' => 'hs_event_new',
            'title' => 'Add a new event',
            'action' => 'hs_eventNew',
            'replyType' => 'skin',
            'file' => 'eventNew.php',
        ),
        
        # View an event
        # @return error | event
        # r=hs_event
        # @id the id of the event
        array(
            'r' => 'hs_event',
            'title' => 'View an event',
            'action' => 'hs_event',
            'access' => 'users_openAccess',
            'replyType' => 'skin',
            'params' => array(
                array('key' => 'id', 'req' => true),
            ),
        ),

        # List stories
        # @return a list of stories
        # r=hs_stories_list
        array(
            'r' => 'hs_stories_list',
            'title' => 'View stories',
            'action' => 'hs_storiesList',
            'replyType' => 'skin',
            'params' => array(
                ['key' => 'name', 'def' => ''],
                ['key' => 'ini', 'def' => '0'],
                ['key' => 'end', 'def' => '10']
            ),
            'file' => 'storiesList.php',
        ),

        #
        # 'Ajax' calls
        #

        # Search for countries (autocomplete)
        # @return a list of countries
        # r=hs_event_new
        array(
            'r' => 'hs_search_country',
            'action' => 'hs_searchCountry',
            'replyType' => 'plain',
            'file' => 'searchCountry.php',
        ),

    );

    return $paths;
}

/**
 * See the front page.
 *
 * External call
 */
function hs_frontPage()
{
    grace_debug('See the front page');

    _hs_scriptsAddAll();

    $stories['events'] = hs_eventsRecentGet();

    $c = skin_this($stories, modules_getPath('hs') . 'skin/frontPage');

    return $c;
}

/**
 * Get a story / view on the screen.
 *
 * External call.
 */
function hs_story($storyId)
{
    grace_debug('View a story');

    _hs_scriptsAddAll();

    $story = hs_storyLoad($storyId);

    if (!$story) {
        return tools_errSet('I could not find that story', '404');
    }

    $story['events'] = hs_eventsLoadList($storyId, 0, 10);

    $c = skin_this($story, modules_getPath('hs') . 'skin/storyMain');

    skin_setTitle($story['title']);

    return $c;
}

/**
 * Get recent events.
 */
function hs_eventsRecentGet(
    $ini = 0,
    $end = 10
) {
    grace_debug('Getting recent events in history');

    $q = sprintf('SELECT el.*, s.title AS storyTitle, s.idStory
		FROM h_eventsLog AS el
		INNER JOIN h_storiesEvents AS se ON se.idEvent = el.idEvent
		INNER JOIN h_stories AS s ON se.idStory = s.idStory
		ORDER BY el.eventDate DESC
		LIMIT %s,%s', $ini, $end);

    $events = db_q($q);

    return $events;
}

/**
 * Get recent stories (latest edits).
 */
function hs_storyRecentLoad(
    $ini = 0,
    $end = 10,
    $order = 'events' /* Order by latest event or creation */
) {
    grace_debug('Loading recent stories');

    $orderBy = 'latestEvent DESC';
    if ($order == 'created') {
        $orderBy = 'created DESC';
    }

    $q = sprintf(
        'SELECT * FROM h_stories ORDER BY %s LIMIT %s,%s',
        $orderBy,
        $ini,
        $end
    );

    $stories = db_querySingle($q);

    return $stories;
}

/**
 * Load a story from the db.
 */
function hs_storyLoad($storyId)
{
    grace_debug('Load a story: ' . $storyId);

    $q = 'SELECT * FROM h_stories WHERE idStory = ' . $storyId;

    $story = db_querySingle($q);

    return $story;
}

/**
 * Load 'all' stories.
 *
 * Internal call ONLY
 */
function hs_storyLoadAll(
    $name = '', /* Filter by name */
    $ini = 0, /* Where to start */
    $end = 10, /* Where to end */
    $status = 1 /* Status of the story use 'false' for all */
) {
    grace_debug('Load a all stories');

    $where = [];
    if ($name != '') {
        $where[] = "title LIKE '%$name%'";
    }
    if ($status != false) {
        $where[] = "status = '$status'";
    }

    $where = 'WHERE ' . implode(' AND ', $where);

    $q = sprintf(
        'SELECT * FROM h_stories %s LIMIT %s,%s',
        $where,
        $ini,
        $end
    );

    return db_q($q);
}


/**
 * Load events from a story.
 */
function hs_eventsLoadList($storyId, $ini = 0, $end = 10)
{
    grace_debug('Load events for a story: ' . $storyId);

    $q = sprintf(
        '
SELECT el.*, se.idStory, hs.title AS storyTitle
FROM `h_eventsLog` AS el
INNER JOIN `h_storiesEvents` AS se
INNER JOIN `h_stories` AS hs
WHERE se.idStory = %s
AND hs.idStory = %s
AND se.idEvent = el.idEvent
LIMIT %s,%s',
        $storyId,
        $storyId,
        $ini,
        $end
    );

    $events = db_q($q);

    return $events;
}

/**
 * Helper function to add scripts to the page.
 */
function _hs_scriptsAddAll()
{

    # I need some md magic
    skin_scriptAdd('web/jquery.js');
    //skin_scriptAdd('web/markdownit/markdown-it.min.js');
    skin_scriptAdd('web/xbbcode.js');
    skin_scriptAdd('web/hs.js', 'file', 'footer');

    skin_scriptAdd('
	$(document).ready(function() {
	hs_parseBbCodes();
	});', 'text', 'footer');
}

/**
 * Load and view an event.
 */
function hs_event($id)
{
    grace_debug('View an event: ' . $id);

    $event = hs_event_load($id);
     
    if (!$event) {
        return tools_errSet(
            'Event not found',
            '404'
         );
    }

    _hs_scriptsAddAll();
     
    $e = skin_this($event, modules_getPath('hs') . 'skin/event');

    skin_setTitle($event['title']);

    return $e;
}

/**
 * Helper function to actually load events.
 */
function hs_event_load($idEvent)
{
    grace_debug('Load an event');

    $q = "SELECT hel.* 
		 FROM `h_eventsLog` AS hel
		 INNER JOIN `h_events` AS he ON he.idCurrent = hel.idLog
		 WHERE hel.idEvent = '$idEvent'";

    return db_querySingle($q);
}
