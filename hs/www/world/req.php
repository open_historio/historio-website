<?php

@header('Content-Type: text/html; charset=utf-8');
@header('Content-Type: application/json');

/*CORS to the app access*/
@header('Access-Control-Allow-Origin: *');
@header('Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
@header('Access-Control-Allow-Methods: POST');

$file = './citiesJs/'.$_GET['file'];

if(file_exists($file)){
	print file_get_contents($file);
}else{
	print 'Life is like a box of chocolates';
}
