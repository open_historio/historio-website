-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 09, 2019 at 11:35 AM
-- Server version: 10.1.41-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `history_devMain`
--

-- --------------------------------------------------------

--
-- Table structure for table `h_events`
--

CREATE TABLE `h_events` (
  `idEvent` int(11) NOT NULL,
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idCreator` int(11) NOT NULL,
  `idCurrent` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `h_eventsLog`
--

CREATE TABLE `h_eventsLog` (
  `idLog` int(10) UNSIGNED NOT NULL,
  `idEvent` int(10) UNSIGNED NOT NULL,
  `idCreator` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idLastEdit` int(11) NOT NULL,
  `ipEdit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eventDate` int(10) UNSIGNED NOT NULL,
  `country` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `h_stories`
--

CREATE TABLE `h_stories` (
  `idStory` int(10) UNSIGNED NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` int(11) NOT NULL,
  `idCreator` int(10) UNSIGNED NOT NULL,
  `lang` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `h_storiesEvents`
--

CREATE TABLE `h_storiesEvents` (
  `idStory` int(10) UNSIGNED NOT NULL,
  `idEvent` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `h_events`
--
ALTER TABLE `h_events`
  ADD PRIMARY KEY (`idEvent`),
  ADD KEY `title` (`title`),
  ADD KEY `idCreator` (`idCreator`);

--
-- Indexes for table `h_eventsLog`
--
ALTER TABLE `h_eventsLog`
  ADD PRIMARY KEY (`idLog`),
  ADD KEY `country` (`country`),
  ADD KEY `region` (`region`),
  ADD KEY `city` (`city`),
  ADD KEY `zip` (`zip`);

--
-- Indexes for table `h_stories`
--
ALTER TABLE `h_stories`
  ADD PRIMARY KEY (`idStory`),
  ADD UNIQUE KEY `idCreator` (`idCreator`),
  ADD KEY `parent` (`parent`),
  ADD KEY `title` (`title`);

--
-- Indexes for table `h_storiesEvents`
--
ALTER TABLE `h_storiesEvents`
  ADD UNIQUE KEY `idStorie` (`idStory`,`idEvent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `h_events`
--
ALTER TABLE `h_events`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `h_eventsLog`
--
ALTER TABLE `h_eventsLog`
  MODIFY `idLog` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `h_stories`
--
ALTER TABLE `h_stories`
  MODIFY `idStory` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
