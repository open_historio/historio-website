-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 09, 2019 at 10:37 PM
-- Server version: 10.1.41-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `history_devMain`
--

-- --------------------------------------------------------

--
-- Table structure for table `h_events`
--

CREATE TABLE `h_events` (
  `idEvent` int(11) NOT NULL,
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idCreator` int(11) NOT NULL,
  `idCurrent` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `h_events`
--

INSERT INTO `h_events` (`idEvent`, `title`, `idCreator`, `idCurrent`) VALUES
(1, 'Linux is announced', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `h_eventsLog`
--

CREATE TABLE `h_eventsLog` (
  `idLog` int(10) UNSIGNED NOT NULL,
  `idEvent` int(10) UNSIGNED NOT NULL,
  `idCreator` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `idLastEdit` int(11) NOT NULL,
  `ipEdit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eventDate` int(10) UNSIGNED NOT NULL,
  `country` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `h_eventsLog`
--

INSERT INTO `h_eventsLog` (`idLog`, `idEvent`, `idCreator`, `title`, `text`, `idLastEdit`, `ipEdit`, `eventDate`, `country`, `region`, `city`, `zip`, `slug`) VALUES
(1, 1, 2, 'Linus Torvalds announces Linux', '[quote]\r\nFrom: torvalds@klaava.Helsinki.FI (Linus Benedict Torvalds)\r\n  Newsgroups: comp.os.minix\r\n  Subject: Gcc-1.40 and a posix-question\r\n  Message-ID:\r\n  Date: 3 Jul 91 10:00:50 GMT\r\n\r\n  Hello netlanders,\r\n\r\n  Due to a project I\'m working on (in minix), I\'m interested in the posix\r\n  standard definition. Could somebody please point me to a (preferably)\r\n  machine-readable format of the latest posix rules? Ftp-sites would be\r\n  nice.\r\n\r\n   The project was obviously linux, so by July 3rd I had started to think\r\n   about actual user-level things: some of the device drivers were ready,\r\n   and the harddisk actually worked. Not too much else.\r\n\r\n  As an aside for all using gcc on minix - [ deleted ]\r\n\r\n   Just a success-report on porting gcc-1.40 to minix using the 1.37\r\n   version made by Alan W Black & co.\r\n\r\n                Linus Torvalds          torvalds@kruuna.helsinki.fi\r\n\r\n  PS. Could someone please try to finger me from overseas, as I\'ve\r\n  installed a \"changing .plan\" (made by your\'s truly), and I\'m not certain\r\n  it works from outside? It should report a new .plan every time.\r\n[/quote]\r\n[url=https://www.cs.cmu.edu/~awb/linux.history.html]https://www.cs.cmu.edu/~awb/linux.history.html[/url]', 2, '127.0.0.0', 1573234766, 'fl', 'Helsinki', 'Helsinki', '0', ''),
(2, 2, 2, 'Carrie Lam (Hong Kong) delivers speech on video after protests in legislature', '[quote]\r\nToday, Chief Executive of Hong Kong Carrie Lam delivered her annual policy speech. Speaking in the Legislative Council, Lam was interrupted by protesting legislators. The council was adjourned and Lam later delivered the speech via video. This was reportedly the first such occurrence.\r\n\r\nLawmakers from the pro-democracy camp shouted at Lam and projected slogans onto the walls of the council chambers. A number of legislators were removed. Council President Andrew Leung adjourned the session mere minutes into the speech as Lam left.\r\n\r\nIn the subsequent video broadcast of the speech, Lam said, \"any acts that advocate Hong Kong’s independence and threaten the country’s sovereignty, security and development interests will not be tolerated.\" She further stated, \"So long as Hong Kong remains impeded by unresolved disputes, ongoing violence, confrontation and discord, our city cannot embark on the road to reconciliation and people will lose faith in the future […] We have to put aside differences and stop attacking each other\". \r\n[/quote]\r\n\r\nhttps://en.wikinews.org/wiki/Hong_Kong%27s_Carrie_Lam_delivers_speech_on_video_after_protests_in_legislature\r\n', 1573322438, '127.0.0.1', 1573322438, 'hk', 'Hong Kong', 'Hong Kong', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `h_stories`
--

CREATE TABLE `h_stories` (
  `idStory` int(10) UNSIGNED NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` int(11) NOT NULL,
  `latestEvent` int(10) UNSIGNED NOT NULL COMMENT 'timestamp of the latest event',
  `idCreator` int(10) UNSIGNED NOT NULL,
  `lang` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `h_stories`
--

INSERT INTO `h_stories` (`idStory`, `parent`, `title`, `about`, `created`, `latestEvent`, `idCreator`, `lang`, `slug`) VALUES
(1, 0, 'Linux Operating System', 'Linux (/ˈlɪnəks/ (About this soundlisten) LIN-əks)[9][10] is a family of open source Unix-like operating systems based on the Linux kernel,[11] an operating system kernel first released on September 17, 1991, by Linus Torvalds.[12][13][14] Linux is typically packaged in a Linux distribution. ', 1573234766, 0, 2, 'en', ''),
(2, 0, 'Hong Kong protests 2019', 'Hong Kong is considered a Special Administrative Region of China.\r\n\r\nIn 2019 a series of events broke in an attempt (possibly) to break free from the chinesse regime/control.', 1573321181, 0, 2, 'en', '');

-- --------------------------------------------------------

--
-- Table structure for table `h_storiesEvents`
--

CREATE TABLE `h_storiesEvents` (
  `idStory` int(10) UNSIGNED NOT NULL,
  `idEvent` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `h_storiesEvents`
--

INSERT INTO `h_storiesEvents` (`idStory`, `idEvent`) VALUES
(1, 1),
(2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `h_events`
--
ALTER TABLE `h_events`
  ADD PRIMARY KEY (`idEvent`),
  ADD KEY `title` (`title`),
  ADD KEY `idCreator` (`idCreator`);

--
-- Indexes for table `h_eventsLog`
--
ALTER TABLE `h_eventsLog`
  ADD PRIMARY KEY (`idLog`),
  ADD KEY `country` (`country`),
  ADD KEY `region` (`region`),
  ADD KEY `city` (`city`),
  ADD KEY `zip` (`zip`),
  ADD KEY `title` (`title`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `h_stories`
--
ALTER TABLE `h_stories`
  ADD PRIMARY KEY (`idStory`),
  ADD KEY `parent` (`parent`),
  ADD KEY `title` (`title`),
  ADD KEY `idCreator` (`idCreator`) USING BTREE,
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `h_storiesEvents`
--
ALTER TABLE `h_storiesEvents`
  ADD UNIQUE KEY `idStorie` (`idStory`,`idEvent`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `h_events`
--
ALTER TABLE `h_events`
  MODIFY `idEvent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `h_eventsLog`
--
ALTER TABLE `h_eventsLog`
  MODIFY `idLog` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `h_stories`
--
ALTER TABLE `h_stories`
  MODIFY `idStory` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
