
<h1 id='storyTitle'>
	<?php print $skin['title']; ?>
</h1>

<div id='storyEvents'>
	<h2>Recent events for this story</h3>
	<?php foreach ($skin['events'] as $event) {
    ?>
	<h3 class='eventTitle'>
		<a href="?w=hs_event&id=<?php print $event['idEvent'] ?>">
		<?php print $event['title'] ?>
		</a>
	</h3>
	<div class='eventDateBrief'>
		<?php print date('Y-m-d', $event['eventDate']); ?>
	</div>
	<div class='eventTextBrief bbCodeMe' id='event_<?php print $event['idEvent']?>'>
		<?php print $event['text'] ?>
	</div>
	<?php
} ?>
</div>
