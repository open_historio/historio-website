<h1 id='pageTitle'>
	Stories with recent activity
</h1>

<div id='storyEvents'>
	<?php foreach($skin['events'] as $event){ ?>

	<h2 class='storyTitle'>
		<a href="?w=hs_story&storyId=<?php print $event['idStory'] ?>">
		<?php print $event['storyTitle'] ?>
		</a>
	</h2>

	<h3 class='eventTitle'>
		<a href="?w=hs_event&id=<?php print $event['idEvent'] ?>">
		<?php print $event['title']; ?>
		</a>
	</h3>

	<div class='eventDateBrief'>
		<?php print date('Y-m-d', $event['eventDate']); ?>
	</div>
	<div class='storyTextBrief bbCodeMe' id='story_<?php print $event['idStory']?>'>
		<?php print $event['text'] ?>
	</div>
	<?php } ?>
</div>


