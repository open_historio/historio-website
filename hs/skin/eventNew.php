<?php print $skin['head'] ?>

<p>Title</p>
<?php print $skin['title'] ?>

<p>Description</p>
<?php print $skin['text'] ?>
<div class="fieldHelp">
You can use <a href="https://www.bbcode.org/reference.php">BBCode</a>.<br />
Add one or more links to stories or resources about this event.
</div>

<br />

<p>Date (dd/mm/yyyy)</p>
<?php print $skin['eventDate'] ?>
<br />

<div class="fieldHelp">
If you can not find the exact region or city, use the closest one.<br />
If the event happened 'online', well, the person was in a country somewhere, use that.
</div>

<p>Country</p>
<div class="ui-widget">
<?php print $skin['country'] ?>
</div>
<br />

<p>Region</p>
<?php print $skin['region'] ?>
<br />

<p>City</p>
<?php print $skin['city'] ?>
<br />

<p>Zip</p>
<?php print $skin['zip'] ?>
<br />

<br />
<?php print $skin['submit'] ?>

<p class="genHelp">
Thanks for helping keep the story of the world.<br />
If you are NOT logged in your IP will be registered.
After you have created this event you will be able to associate it with one or more stories.
</p>

</form>
