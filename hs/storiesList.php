<?php

/**
 * View a list of stories
 */
function hs_storiesList(
	$name, /* Filter by name */
	$ini = 0, /* Where to start */
	$end = 10 /* Where to end */
){

	grace_debug('Get a list of stories');

	$s = hs_storyLoadAll($name, $ini, $end);

	$c = skin_this(['stories' => $s], modules_getPath('hs') . 'skin/storiesList');

	return $c;

}
