<?php

/**
 * Generate a form to add a new story.
 */
function hs_storyNew(){

	grace_debug('Get form to ad a new story: ' . $action);

	$form = array(
		'form' =>
		array(
			'method' => 'POST',
			'action' => '?w=hs_story_new&form=hs_story_new',
			'id' => 'hs_story_new',
			'table' => 'h_stories'
		),
		'fields' =>
			array(
				'title' => array(
					'name' => 'Story title',
					'type' => 'input',
					'maxsize' => '100',
					'required' => true,
				),
				'about' => array(
					'name' => 'Story description',
					'type' => 'textarea',
					'required' => true
				),
				'submit' => array(
					'type' => 'submit',
					'value' => 'Start a new story!'
				)
			)
		);

	# Parse and skin the form
	$form = forms_get($form, modules_getPath('hs') . 'skin/storyNew');

	return $form;
}

