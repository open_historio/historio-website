<?php

/**
 * Generate a form to add a new story.
 */
function hs_eventNew()
{
    grace_debug('Get form to ad a new story');

    $form = array(
        'form' =>
        array(
            'method' => 'POST',
            'action' => '?w=hs_event_new&form=hs_event_new',
            'id' => 'hs_event_new',
            'table' => 'h_eventsLog'
        ),
        'fields' =>
            array(
                'title' => array(
                    'name' => 'Event title',
                    'type' => 'input',
                    'maxsize' => '100',
                    'required' => true,
                ),
                'text' => array(
                    'name' => 'Event description',
                    'type' => 'textarea',
                    'rows' => 5,
                    'required' => true
                ),
                'eventDate' => array(
                    'name' => 'Event date (dd/mm/yyyy)',
                    'type' => 'input',
                    'maxsize' => '10',
                    'required' => true,
                ),
                'country' => array(
                    'name' => 'Country',
                    'type' => 'select',
                    'required' => true,
                ),
                'region' => array(
                    'name' => 'Region',
                    'type' => 'select',
                    'required' => true,
                ),
                'city' => array(
                    'name' => 'City',
                    'type' => 'select',
                    'required' => true,
                ),
                'zip' => array(
                    'name' => 'Zip Code',
                    'type' => 'input',
                    'maxsize' => '100',
                    'required' => false,
                ),
                'submit' => array(
                    'type' => 'submit',
                    'value' => 'Start a new story!'
                )
            )
        );

    # Parse and skin the form
    $form = forms_get($form, modules_getPath('hs') . 'skin/eventNew');

    skin_scriptAdd('web/jquery.js');
    skin_scriptAdd('web/world/countries.js');
    skin_scriptAdd('web/hs.js');

    skin_scriptAdd('web/jqueryUi/jquery-ui.min.js');
    skin_cssAdd('web/jqueryUi/jquery-ui.min.css');
    skin_cssAdd('web/jqueryUi/jquery-ui.theme.min.css');

    skin_scriptAdd('
console.log("Autocomplete for countries");

// Generate country list
hs_countriesGen("#country");

cala_worldSetWorldForChange();
cala_worldSetRegionForchange();

// Date picker
  $(function(){
$("#eventDate").datepicker({
  dateFormat: "dd/mm/yy"
});
  });

', 'text', 'footer');

    return $form;
}

/**
 * Check the form.
 */
function hs_event_new_check($form)
{
    grace_debug('Checking form with my function');

    if ($form['submitted'] == 'good') {
        global $user;
        # I need a unix timestamp
        list($d, $m, $y) = explode('/', $form['fields']['eventDate']['value']);
        $form['fields']['eventDate']['value'] = mktime(0, 0, 0, $m, $d, $y);
        $form['fields']['idCreator']['value'] = $user['idUser'];
        $form['fields']['editLog']['value'] = md5(time() * rand(3, 2854675485));
    }

    return $form;
}

/**
 * The form was inserted.
 */
function hs_event_new_inserted($form)
{
    grace_debug('Form was inserted');

    # I need to get the form to create the h_event entry
    $q = sprintf(
        'SELECT idLog, title, idCreator FROM `h_eventsLog` WHERE editLog = \'%s\'',
        $form['fields']['editLog']['value']
    );

    $lastEvent = db_querySingle($q);

    if (!$q) {
        grace_error('Something bad happened, I should probably log this');
        return $form;
    }

    # @todo check if user has permissions and set the status accordingly
    # @todo add a slug
    $q = sprintf(
        'INSERT INTO `h_events` (title, idCreator, idCurrent, status, slug)
	  	VALUES(\'%s\',\'%s\',\'%s\',\'%s\',\'%s\')',
        $lastEvent['title'],
        $lastEvent['idCreator'],
        $lastEvent['idLog'],
        1,
        ''
        );

    db_exec($q);

    return $form;
}
